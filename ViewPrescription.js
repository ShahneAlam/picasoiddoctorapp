import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Alert,
    TouchableOpacity,
    TextInput,
    Image,
    ImageBackground,
    Linking,
    FlatList,
    Dimensions,
    ActivityIndicator
} from 'react-native';
const window = Dimensions.get('window');
const GLOBAL = require('./Global');
import React, {Component} from 'react';
import Button from 'react-native-button';
import ImagePicker from 'react-native-image-picker';
import Header from './Header.js';
var finalImagesName = '';

class ViewPrescription extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            avatarSource: null,
            image:'',
            addmore:[],
            startedadd:0,
            loading:false,
            up_images_name:'',
        }

        this.selectPhotoTapped = this.selectPhotoTapped.bind(this);


    }

    componentDidMount(){
        const url = GLOBAL.BASE_URL +  'get_prescription_data'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "doctor_id":GLOBAL.user_id,
                "booking_id":GLOBAL.order_id,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {

               console.log(JSON.stringify(responseJson))
            if(responseJson.status == true){
               this.setState({addmore:responseJson.prescription_content})


            }else{

                // alert('Something went wrong!')

            }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

    }


    componentWillUnmount(){
        GLOBAL.profile =''
        GLOBAL.finalImagesName=''
    }

        finalUploadAllImages = ()=> {
            console.log(GLOBAL.finalImagesName + GLOBAL.order_id + GLOBAL.user_id)
        const url = GLOBAL.BASE_URL + 'update_prescription_data'

        var updatefinalImages =''

        updatefinalImages =  this.state.addmore[0].image_name
        for(let i = 1 ; i< this.state.addmore.length ; i++ ){
            updatefinalImages = updatefinalImages + '|' + this.state.addmore[i].image_name
        }

        console.log(JSON.stringify({
                "doctor_id":GLOBAL.user_id,
                "booking_id":GLOBAL.order_id,
                "prescription_type":"1",
                "prescription_content": updatefinalImages,
                "prescription_content_new": GLOBAL.finalImagesName
            }))

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({
                "doctor_id":GLOBAL.user_id,
                "booking_id":GLOBAL.order_id,
                "prescription_type":"1",
                "prescription_content": updatefinalImages,
                "prescription_content_new": GLOBAL.finalImagesName
            }),
        }).then((response) => response.json())
            .then((responseJson) => {

//                alert(JSON.stringify(responseJson))
                if(responseJson.status == true){
//                this.setState({list:responseJson.lists})
                    alert('Prescription updated successfully!')
                    this.props.navigation.goBack()

            }else{

                alert('Something went wrong!')

            }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

    }

    uploadSingleImage = (geturi)=> {
        this.showLoading()

        const url = GLOBAL.BASE_URL +'image_attchment_upload_doctor_2'
        const data = new FormData();
        data.append('user_id', GLOBAL.user_id);
        data.append('flag', "1");
        data.append('booking_id', GLOBAL.order_id);
        data.append('images_name', this.state.up_images_name);
        data.append('image', {
            uri: geturi,
            type: 'image/jpeg', // or photo.type
            name: 'image.png'
        });
        fetch(url, {
            method: 'post',
            body: data,
            headers: {
                'Content-Type': 'multipart/form-data',
            }

        }).then((response) => response.json())
            .then((responseJson) => {
              console.log(JSON.stringify(responseJson))
                this.hideLoading()
                if(responseJson.status == true){
//                    alert(responseJson.images[0].image)
                    finalImagesName =  responseJson.images[0].image
                    for(let i = 1 ; i< responseJson.images.length ; i++ ){
                        finalImagesName = finalImagesName + '|' + responseJson.images[i].image
                    }

                GLOBAL.finalImagesName = finalImagesName
                console.log(GLOBAL.finalImagesName)
                
                const theOneIWant = [...responseJson.images].pop();

                // alert(theOneIWant.image)

                this.setState({image : geturi,
                    up_images_name: theOneIWant.image
                })

                this.setState({startedadd :1})
                this._handleAddButton(geturi)
                }else{
                alert('Something went wrong!')
                }

            });

        }

    static navigationOptions = ({ navigation }) => {
        return {
              header: () => null,

        }
    }


    showLoading() {
        this.setState({loading: true})
    }

    hideLoading() {
        this.setState({loading: false})
    }


    selectPhotoTapped=()=> {

        const options = {
            title: 'Select Prescription Image',
            storageOptions: {
                skipBackup: true,
                maxWidth : 500,
                maxHeight : 500,                
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
//            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
//                GLOBAL.profile = source.uri
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

//                this.setState({image :   GLOBAL.profile})
                //    alert(JSON.stringify(source))
                this.uploadSingleImage(source.uri)
            }
        });
    }



_handleAddButton(imageuri) {
    let newly_added_data = { is_selected:0, image: imageuri, image_name: this.state.up_images_name };

    this.setState({
        addmore: [...this.state.addmore, newly_added_data]
    });
}

    ondelete=(item, index)=>{
       var arr= this.state.addmore
       arr.splice(index, 1)
       // alert(JSON.stringify(arr))
       alert('Prescription image removed!')
       this.setState({addmore: arr})
    }


    renderItem1=({item, index})=>{
        if(item.plusImage){
            return(
            <Button style={{alignSelf:'center',fontSize:12,color:'white',fontFamily:'Konnect-Medium'}}
                    containerStyle={{height:30,width:100,marginLeft:15,marginTop:10,borderRadius:15,backgroundColor:'#800000',justifyContent:'center'}}
                    onPress={this.selectPhotoTapped.bind(this)}>
                Add More
            </Button>

                )
        }

        return(
                    <View style={{height:220,width:Dimensions.get('window').width - 30,borderWidth:1,marginLeft:15,marginTop:15,borderStyle:'dashed',borderRadius:5,borderColor:'grey',justifyContent:'center',flexDirection:'row'}}>

                        <Image source={require('./uploadlogo.png')}
                               style={{alignSelf:'center', height:40,width:40}}/>
                        <Text style={{alignSelf:'center',marginLeft:5,fontFamily:'Konnect-Medium'}}>Upload Prescription</Text>

                        {item.is_selected!='0' && (
                            <View style={{height:220,width:window.width - 25,borderWidth:1,borderColor:'grey',borderRadius:5,position:'absolute',top:15,left:15,justifyContent:'center',flexDirection:'row'}}>




                            </View>
                        )}

                        {item.is_selected=='0' && (
                            <Image    source={{ uri: item.image}}
                                   style={{height:220,width:window.width - 25,position:'absolute',borderColor:'grey',borderRadius:5}}/>

                        )}

                        {index!=0 &&(
                            <TouchableOpacity style={{position:'absolute', top:10, right:5}}
                            onPress={()=> this.ondelete(item, index)}>
                            <Image style={{width:50, height:35, resizeMode:'contain'}}
                            source={require('./trash.png')}/>
                            </TouchableOpacity>
                            )}

                    </View>                    


            )
    }

    render() {

        if(this.state.loading){
            return(
                <View style={{flex:1}}>
                    <ActivityIndicator style = {{
        position: 'absolute',
        left: window.width/2 - 30,
        top: window.height/2,
        opacity: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
}}
                                       size="large" color='#800000' />
                </View>
            )
        }



        return(
            <View style={{flex:1}}>
            <Header navigation={this.props.navigation}
            headerName={'VIEW PRESCRIPTION'}/>


                <FlatList style={{marginBottom:10}}
                    data={[...this.state.addmore, {plusImage: true}]}
                    keyExtractor={this._keyExtractor1}
                    renderItem={this.renderItem1}
                    extraData={this.state}
                />

  



    <Button style={{fontSize:17,color:'white',fontFamily:'Konnect-Medium',}}
            onPress={() => {
                this.finalUploadAllImages()
            }}

            containerStyle={{height:60,borderRadius:15,backgroundColor:'#800000',justifyContent:'center',margin:20}}>
        UPDATE
    </Button>

            </View>
        );
    }
}

export default ViewPrescription;